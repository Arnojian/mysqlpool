#include<stdlib.h>
#include<stdio.h>
#include<fcntl.h>
//#include<sys/types.h>
#include<mysql_db.h>

static int dealStrLine(const char *pStrLine)
{
#define HOST "HOST="
#define PORT "PORT="
#define USER "USER="
#define PASSWD	"PASSWD="
#define DBNAME	"DBNAME="
#define HANDLENUM	"HANDLE_NUM="

	DBConfig_t *pDBConfig = getDBConfigPtr() ;
	if(pStrLine ==NULL)
		return RET_ERR ;
	char *p = NULL ;
	if((p = strstr(pStrLine,HOST))!=NULL)
	{
		//strcpy(pDBConfig->host[0],p+strlen(HOST)) ;
		strncpy(pDBConfig->host[0],p+strlen(HOST),strlen(p+strlen(HOST))-1) ; //��ȥ���з�
		printf("HOST:%s\n",pDBConfig->host[0]) ;
	}
	else if((p=strstr(pStrLine,PORT))!=NULL)
	{
		pDBConfig->port[0]=atoi(p+strlen(PORT));
		printf("port:%d\n",pDBConfig->port[0]) ;
	}
	else if((p=strstr(pStrLine,USER))!=NULL)
	{
		//strcpy(pDBConfig->user[0],p+strlen(USER)) ;
		strncpy(pDBConfig->user[0],p+strlen(USER),strlen(p+strlen(USER))-1) ;
		printf("user:%s\n",pDBConfig->user[0]) ;
	}
	else if((p=strstr(pStrLine,PASSWD))!=NULL)
	{
		//strcpy(pDBConfig->passwd[0],p+strlen(PASSWD)) ;
		strncpy(pDBConfig->passwd[0],p+strlen(PASSWD),strlen(p+strlen(PASSWD))-1) ;
		printf("passwd:%s\n",pDBConfig->passwd[0]) ;
	}
	else if((p=strstr(pStrLine,DBNAME))!=NULL)
	{
		//strcpy(pDBConfig->dbName[0],p+strlen(DBNAME)) ;
		strncpy(pDBConfig->dbName[0],p+strlen(DBNAME),strlen(p+strlen(PASSWD))-1) ;
		printf("dbName:%s\n",pDBConfig->dbName[0]) ;
	}
	else if((p=strstr(pStrLine,HANDLENUM))!=NULL)
	{
		pDBConfig->handle_Num[0]=atoi(p+strlen(HANDLENUM));
		printf("handlenum:%d\n",pDBConfig->handle_Num[0]) ;
	}
	else
	{
		printf("something cannot identify!") ;
		return RET_ERR ;
	}
	return RET_OK ;
}


static int readConfig(const char *pConfigFile)
{
	FILE* fd = -1 ;
	char pStrLine[1024] ;

	fd = fopen(pConfigFile,"r") ;

	while(!feof(fd))
	{
		fgets(pStrLine,1024,fd) ;
		if(pStrLine!=NULL || *pStrLine != " "|| *pStrLine !="\r" || *pStrLine != "\n" )
		{
			dealStrLine(pStrLine) ;
		}

	}
	fclose(fd) ;
	printf("readConfig is ok\n") ;
	return RET_OK;
}

int main(int argc,char *argv[])
{
	int ret = 0 ;
	const char *sql_select="select * from user_info limit 1" ;
	const char *sql_insert="insert into user_info(name,sex) values('luojian',1)" ;
	MYSQL_RES *resRecord = NULL ;
	int effectRows = 0 ;
	if(argc ==1)
	{
		printf("please input the path of db config file!") ;
		return -1 ;
	}

	ret = readConfig(argv[1]) ;
	if(ret != RET_OK)
	{
		printf("readConfig is error,please check the config file!") ;
		return -1 ;
	}

	DB_Init();

	ret = DB_Insert_Update_Del(0,sql_insert,&effectRows) ;
	if(ret==RET_OK)
	{
		printf("DB_Insert_Update_Del is ok,execute:%s,effectRows:%d",
				sql_insert,effectRows);
	}
	ret = DB_Select(0,sql_select,&resRecord) ;
	if(ret == RET_OK)
	{
		printf("DB_Select is ok") ;
		mysql_free_result(resRecord) ;
		return RET_OK ;
	}
	DB_End() ;
	return 0 ;
}

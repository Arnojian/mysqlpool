/*
 * mysql_db.c
 *
 *  Created on: 2016��10��14��
 *      Author: arno.luo
 */

#include<mysql_db.h>
#include<stdio.h>
#include<stdint.h>
#include<pthread.h>

static DBConfig_t gDBConfig;
static DBHandle_t *gDBHandle[DB_MAX];
int gDBHandleNum[DB_MAX] ; //the number handle is read from config.
static pthread_mutex_t gDB_Mutex[DB_MAX] ;

static int DB_Close(MYSQL **conn);
static int DB_CreateConn(int db_Index,MYSQL **conn);
static int DB_Exe(MYSQL** conn,const char *sql) ;
static int DB_Mutex_Init() ;
static int DB_Mutex_Destroy() ;

DBConfig_t * getDBConfigPtr()
{
	return &gDBConfig ;
}

static int DB_Mutex_Init()
{
	//init the mutex of db
	int dbIndex = 0 ;
	for(dbIndex=0;dbIndex<DB_MAX;dbIndex++)
	{
		pthread_mutex_init(&gDB_Mutex[dbIndex],NULL);
	}
	return RET_OK ;
}
static int DB_Mutex_Destroy()
{
	int dbIndex = 0 ;
	for(dbIndex=0;dbIndex<DB_MAX;dbIndex++)
	{
		pthread_mutex_destroy(&gDB_Mutex[dbIndex]) ;
	}
	return RET_OK ;
}
int DB_Init()
{
	int index = 0 ;
	int dbIndex = 0 ;
	MYSQL *handle=NULL ;
	int ret = RET_ERR ;

	DB_Mutex_Init() ;

	//init the handle of db
	for(dbIndex=0;dbIndex<DB_MAX;dbIndex++)
	{
		gDBHandleNum[dbIndex]=gDBConfig.handle_Num[dbIndex] ;
		gDBHandle[dbIndex] = (DBHandle_t*)malloc(gDBHandleNum[dbIndex]*sizeof(DBHandle_t)) ;
		if(!gDBHandle[dbIndex])
		{
			fprintf(stderr,"malloc DBHandle is error\n") ;
			DB_Mutex_Destroy() ;
			return RET_ERR ;
		}
		for(index=0;index < gDBHandleNum[dbIndex];index++)
		{
			gDBHandle[dbIndex][index].dbHandle = NULL ;
			gDBHandle[dbIndex][index].status = STATUS_DBHANDLE_IDE_E ;
			ret = DB_CreateConn(dbIndex,&gDBHandle[dbIndex][index].dbHandle);
			if(RET_OK!=ret)
			{
				DB_Mutex_Destroy() ;
				return RET_ERR ;
			}
		}//end for index
	}//end for dbindx
	printf("db_Init is ok\n") ;
	return RET_OK ;
}


MYSQL** DB_GetAvailableHandle(int db_Index,int *pIndex)
{
	int index = 0 ;
	MYSQL *conn=NULL ;
	MYSQL **dbHandle=NULL ;
	int cnt = 3 ;//try three times
	int ret = RET_ERR ;
	Status_Handle_e flag ;
	while(cnt--){
		//pthread_cleanup_push(pthread_mutex_unlock,(void*)&gDB_Mutex[db_Index]) ;
		pthread_mutex_lock(&gDB_Mutex[db_Index]);
		for(index=0 ;index < gDBHandleNum[db_Index];index++)
		{
			flag = gDBHandle[db_Index][index].status ;
			conn = gDBHandle[db_Index][index].dbHandle ;
			if(flag== STATUS_DBHANDLE_IDE_E)
			{
				gDBHandle[db_Index][index].status = STATUS_DBHANDLE_BUSY_E ;
				dbHandle = &gDBHandle[db_Index][index].dbHandle ;
				if(conn)
				{
					ret = mysql_ping(conn) ;
					if(0==ret){
						*pIndex = index ;
						pthread_mutex_unlock(&gDB_Mutex[db_Index]) ;
						printf("connect index :%d is available",index) ;
						//pthread_cleanup_pop(0) ;
						return dbHandle;
					}
					else
					{
						ret = DB_CreateConn(db_Index,dbHandle) ;
						if(ret==RET_OK)
						{
							*pIndex=index ;
							pthread_mutex_unlock(&gDB_Mutex[db_Index]) ;
							printf("connect index :%d is available",index) ;
							//pthread_cleanup_pop(0) ;
							return dbHandle ;
						}
						else
						{
							gDBHandle[db_Index][index].status = STATUS_DBHANDLE_IDE_E ;
						}
					}
				} //enf if conn
			}
		}//end for index
		pthread_mutex_lock(&gDB_Mutex[db_Index]) ;
		printf("try times :%d",cnt) ;
		//pthread_cleanup_pop(0) ;
	}//end while(cnt--)
	printf("cannot get available handle from dbHandles") ;
	return NULL;
}

int DB_ReleaseHandle(int dbIndex,int hIndex)
{
	if(hIndex<0|| hIndex > gDBHandleNum[dbIndex])
	{
		fprintf(stderr,"DB_ReleaseHandle hIndex is error index\n") ;
		return RET_ERR ;
	}

	pthread_cleanup_push(pthread_mutex_unlock,&gDB_Mutex[dbIndex]) ;
	pthread_mutex_lock(&gDB_Mutex[dbIndex]) ;

	gDBHandle[dbIndex][hIndex].status = STATUS_DBHANDLE_IDE_E ;

	pthread_mutex_unlock(&gDB_Mutex[dbIndex]) ;
	pthread_cleanup_pop(0) ;

	printf("DB_ReleaseHandle dbIndex:%d,hIndex:%d",dbIndex,hIndex) ;
	return RET_OK ;
}

static int DB_Exe(MYSQL **conn,const char *sql)
{
	int ret = RET_ERR ;
	if(!conn || !sql)
		return RET_ERR ;

	ret = mysql_query(*conn,sql) ; //query
	if(ret)
	{
		fprintf(stderr,"mysql_query %s is error:%s",sql,mysql_error(*conn)) ;
		return RET_ERR ;
	}
	printf("DB_Exe:%s",sql) ;
	return RET_OK ;
}

int DB_Insert_Update_Del(int dbIndex,char *sql,int *pEffectRow)
{
	int ret =RET_ERR ;
	int hIndex = -1 ;
	MYSQL **conn = NULL ;
	*pEffectRow = 0 ;
	conn = DB_GetAvailableHandle(dbIndex,&hIndex) ;
	if(NULL==conn)
	{
		fprintf(stderr,"Cannot get the Avavaiblae DB Handle!") ;
		return RET_ERR ;
	}

	ret = DB_Exe(conn,sql) ;
	if(ret==RET_OK)
	{
		*pEffectRow = mysql_affected_rows(*conn) ;//get effect rows
	}
	DB_ReleaseHandle(dbIndex,hIndex) ;

	printf("DB_Insert_Update_Del sql:%s is ok!",sql) ;
	return RET_OK ;
}

int DB_Select(int dbIndex,char *sql,MYSQL_RES **ptr_res)
{
	int ret = RET_ERR ;
	MYSQL **conn =NULL ;
	int hIndex =-1 ;
	if(!sql || !ptr_res)
		return RET_ERR ;

	conn = DB_GetAvailableHandle(dbIndex,&hIndex);
	if(NULL==conn)
	{
		fprintf(stderr,"Cannot get the available") ;
		return RET_ERR ;
	}

	ret =DB_Exe(conn,sql) ;
	if(RET_OK == ret)
	{
		*ptr_res = mysql_store_result(*conn) ; //get datas from mysql
	}

	DB_ReleaseHandle(dbIndex,hIndex) ;

	printf("DB_Select execute sql:%s is ok",sql) ;
	return RET_OK ;
}
static int DB_CreateConn(int db_Index,MYSQL **conn)
{
	int ret = RET_OK ;
	uint8_t timeout = 2 ;
	*conn = mysql_init(NULL) ;

	if(*conn == NULL)
	{
		printf("mysql_init is error") ;
		return RET_ERR ;
	}
	//setting read timeout=2
	mysql_options(*conn,MYSQL_OPT_READ_TIMEOUT,(const char *)&timeout) ;
	//setting write timeout=2
	mysql_options(*conn,MYSQL_OPT_WRITE_TIMEOUT,(const char *)&timeout) ;

	printf("host:%s,user:%s,passwd:%s,dbName:%s,port:%d\n",gDBConfig.host[db_Index],gDBConfig.user[db_Index],gDBConfig.passwd[db_Index],
			gDBConfig.dbName[db_Index],gDBConfig.port[db_Index]);

	ret = mysql_real_connect(*conn,gDBConfig.host[db_Index],gDBConfig.user[db_Index],
			gDBConfig.passwd[db_Index],gDBConfig.dbName[db_Index],gDBConfig.port[db_Index],NULL,0
			) ;
	if(!ret)
	{
		printf("Failed to connect to database:	Error:%s\n",mysql_error(*conn))  ;
		DB_Close(conn) ;
		return ret ;
	}
	printf("createConnect is ok") ;
	return RET_OK;
}

static int DB_Close(MYSQL **conn)
{
	if(*conn)
	{
		mysql_close(*conn) ;
		*conn=NULL ;
		printf("DB:close is ok\n");
	}
	return RET_OK ;
}

int DB_End()
{
	int index = 0 ;
	int dbIndex = 0 ;
	MYSQL **conn = NULL ;
	DB_Mutex_Destroy() ;
	for(dbIndex=0;dbIndex<DB_MAX;dbIndex++)
	{
		for(index=0;index<gDBHandleNum[dbIndex];index++)
		{
			conn = &gDBHandle[dbIndex][index].dbHandle ;
			DB_Close(conn) ;
		}
		free(gDBHandle[dbIndex]);
	}
	printf("DB_End\n") ;
	return RET_OK ;
}



/*
 * mysql_db.h
 *
 *  Created on: 2016��10��14��
 *      Author: arno.luo
 */

#include<mysql/mysql.h>

#define RET_ERR   -1
#define RET_OK    0
#define RET_OTHER 1

#define DB_MAX 		 1
#define HOST_MAX_LEN 512
#define NAME_MAX_LEN 128

typedef enum Status_Handle{
	STATUS_DBHANDLE_IDE_E=0,
	STATUS_DBHANDLE_BUSY_E
}Status_Handle_e;

typedef enum DBIndex{
	DB_MAIN=0,
	DB_TEAT
}DBIndex_e;

//The config of databases;
typedef struct DBConfig{
	char 	host[DB_MAX][HOST_MAX_LEN] ;	//host
	char 	user[DB_MAX][NAME_MAX_LEN] ;	//db user
	char 	passwd[DB_MAX][NAME_MAX_LEN] ;	//db password
	char 	dbName[DB_MAX][NAME_MAX_LEN] ;  //db name
	short 	port[DB_MAX] ;					//db port
	int 	handle_Num[DB_MAX] ;			//handle numbers

}DBConfig_t;

// connection handle
typedef struct DBHandle{
	MYSQL *dbHandle ;		//handle of db
	Status_Handle_e status ;	//the status of dbHandle
}DBHandle_t;
int DB_ReleaseHandle(int dbIndex,int hIndex);
MYSQL** DB_GetAvailableHandle(int db_Index,int *pIndex);
int DB_Insert_Update_Del(int dbIndex,char *sql,int *pEffectRow) ;
int DB_Select(int dbIndex,char *sql,MYSQL_RES **ptr_res) ;
DBConfig_t * getDBConfigPtr() ;
int DB_End();
int DB_Init();
